#include <cheerp/clientlib.h>
#include <cheerp/client.h>
#include <cheerp/types.h>

#include <algorithm>


		
			
/***
 *  author: agent.zy@aliyun.com
 */

using namespace client;
 
namespace [[cheerp::genericjs]] client {
	Window * window_get_Window();
    CustomEvent * window_getCustomEvent (const String & name);
    CustomEvent * window_getCustomEvent (const String & name,  Object * param);
};


#define MY_EVENT_PUB "pub"
 
class [[cheerp::genericjs]] TestEvent {
	private :
	static void MyEventPubHandler(Object *e) {
		console.log("i am catch this event");
	}		
	public:
	TestEvent() {
	}
	
	static void init () {
		Window * window = window_get_Window();
		window->addEventListener(MY_EVENT_PUB, cheerp::Callback(MyEventPubHandler));
		
		Event  * event  = window_getCustomEvent(MY_EVENT_PUB);
		window->dispatchEvent(event);
	}
};


void webMain () { //wasm 函数
	TestEvent::init();
}


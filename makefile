
CHERRP_PATH=/cygdrive/c/cheerp/include
SOURCE=main.cpp
CHEERP_FLAG=-target cheerp -w
WASMFLAGS=-cheerp-linear-heap-size=256 -cheerp-linear-heap-size=256 -cheerp-mode=wasm -cheerp-no-math-imul
WASM_LOADER=-cheerp-wasm-loader=loader.js -cheerp-wasm-file=./build/test.wasm 
CC=clang++
DIST_JS=./build/test.js
DIST_WASM=./build/test_wasm.wasm
clear:
	@rm -f ./build/*

test.wasm:
	$(CC) $(CHEERP_FLAG) $(WASMFLAGS) $(WASM_LOADER) -O3 -o $(DIST_WASM)  main.cpp 

test.js:
	$(CC) $(CHEERP_FLAG) -O3 -o $(DIST_JS) main.cpp 
	cat head.js >> $(DIST_JS)
test:
	node -wasm-opt loader.js 
	
all: clear  test.js test.wasm
	@echo "build done"
